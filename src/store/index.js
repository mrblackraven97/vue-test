// import Vue from 'vue';
import Vuex from "vuex";

import cone from "./cone.js";
import view from "./view.js";

/* eslint-enable no-param-reassign */

function createStore() {
  return new Vuex.Store({
    state: {
      dark: false
    },
    modules: {
      cone,
      view
    },
    getters: {
      APP_DARK_THEME(state) {
        return state.dark;
      }
    },
    mutations: {
      APP_DARK_THEME_SET(state, isDark) {
        state.dark = isDark;
      }
    }
  });
}

export default createStore;
