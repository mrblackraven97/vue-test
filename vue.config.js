const vtkChainWebpack = require("vtk.js/Utilities/config/chainWebpack");

module.exports = {
  chainWebpack: config => {
    // Add project name as alias
    config.resolve.alias.set("vue-test", __dirname);

    // Add vtk.js rules
    vtkChainWebpack(config);
  },
  publicPath: process.env.NODE_ENV === "production" ? `/${process.env.CI_PROJECT_NAME}/` : "/"
};
